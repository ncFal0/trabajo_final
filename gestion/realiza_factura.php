<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Realizar factura</title>
</head>
<body>
<?php
if(isset($_GET['listar']))
{
	echo '<form action="ver_factura.php" method="POST">';
}
else
{
	echo '<form action="alta_factura.php" method="POST">';
}
?>
		<label for="cod_cliente">Factura de cliente: </label>
		<select id="cod_cliente" name="cod_cliente" required>
<?php
foreach($_SESSION['datos']['cliente'] as $cliente)
{
	if(count(buscar($cliente['cod_cliente'], 'cod_cliente', $_SESSION['datos']['albaran'])) > 0)
	{
		echo "<option value='{$cliente['cod_cliente']}'>{$cliente['nombre']}</option>";
	}
}
?>
		</select>
		<input type="submit" value="Aceptar">
	</form>

	<?=muestra_volver('menu.php')?>
</body>
</html>
