<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Realizar pedido</title>
</head>
<body>
<?php
if(isset($_GET['listar']))
{
	echo '<form action="ver_pedido.php" method="POST">';
}
else
{
	echo '<form action="alta_pedido.php" method="POST">';
}
?>
		<label for="cod_cliente">Pedido de cliente: </label>
		<select id="cod_cliente" name="cod_cliente" required>
<?php
foreach($_SESSION['datos']['cliente'] as $cliente)
{
	echo "<option value='{$cliente['cod_cliente']}'>{$cliente['nombre']}</option>";
}
?>
		</select>
		<input type="submit" value="Aceptar">
	</form>

	<?=muestra_volver('menu.php')?>
</body>
</html>
