<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion'])) {
	header('location:../login.php');
	exit();
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Menu</title>
</head>
<body>
	<h2>Usuario Gestión</h2>
	<form action="ver_usuario_gestion.php" method="POST">
		<input type="submit" name="ver_usuario_gestion" value="Ver Usuario Gestion">
	</form>

	<form action="alta_usuario_gestion.php" method="POST">
		<input type="submit" name="alta_usuario_gestion" value="Alta Usuario Gestion">
	</form>
	<hr>

	<h2>Cliente</h2>
	<form action="ver_cliente.php" method="POST">
		<input type="submit" name="ver_cliente" value="Ver Cliente">
	</form>

	<form action="alta_cliente.php" method="POST">
		<input type="submit" name="alta_cliente" value="Alta Cliente">
	</form>
	<hr>

	<h2>Articulo</h2>
	<form action="ver_articulo.php" method="POST">
		<input type="submit" name="ver_articulo" value="Ver Articulo">
	</form>

	<form action="alta_articulo.php" method="POST">
		<input type="submit" name="alta_articulo" value="Alta Articulo">
	</form>
	<hr>

	<h2>Pedido</h2>
	<form action="realiza_pedido.php?listar=1" method="POST">
		<input type="submit" name="ver_pedido" value="Ver Pedido">
	</form>

	<form action="realiza_pedido.php" method="POST">
		<input type="submit" name="realiza_pedido" value="Realizar Pedido">
	</form>
	<hr>

	<h2>Albaran</h2>
	<form action="realiza_albaran.php?listar=1" method="POST">
		<input type="submit" name="ver_albaran" value="Ver Albaran">
	</form>

	<form action="realiza_albaran.php" method="POST">
		<input type="submit" name="realiza_albaran" value="Realizar Albaran">
	</form>
	<hr>

	<h2>Factura</h2>
	<form action="realiza_factura.php?listar=1" method="POST">
		<input type="submit" name="ver_factura" value="Ver Factura">
	</form>

	<form action="realiza_factura.php" method="POST">
		<input type="submit" name="realiza_factura" value="Realizar Factura">
	</form>
	<hr>

	<form action="../guardar.php" method="POST">
		<input type="submit" name="guardar" value="Guardar Cambios">
	</form>

	<form action="../comprueba_usuario_gestion.php" method="POST">
		<input type="submit" name="cerrar_sesion" value="Cerrar Sesión">
	</form>
</body>
</html>

