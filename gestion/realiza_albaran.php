<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Realizar albaran</title>
</head>
<body>
<?php
if(isset($_GET['listar']))
{
	echo '<form action="ver_albaran.php" method="POST">';
}
else
{
	echo '<form action="alta_albaran.php" method="POST">';
}
?>
		<label for="cod_pedido">Albarán de pedido: </label>
		<select id="cod_pedido" name="cod_pedido" required>
<?php
foreach($_SESSION['datos']['cliente'] as $cliente)
{
	$pedidos_cliente = buscar($cliente['cod_cliente'], 'cod_cliente', $_SESSION['datos']['pedido']);


	if(count($pedidos_cliente) > 0)
	{
		echo "<optgroup label='Cliente: {$cliente['nombre']}'>";
		foreach($pedidos_cliente as $pedido)
		{
			echo "<option value='{$pedido['cod_pedido']}'>Pedido #{$pedido['cod_pedido']}</option>";
		}
		echo '</optgroup>';
	}
}
?>
		</select>
		<input type="submit" value="Aceptar">
	</form>

	<?=muestra_volver('menu.php')?>
</body>
</html>
