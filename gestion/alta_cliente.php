<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

$cadena_resultado = "";

if(isset($_POST['nombre'], $_POST['nick'], $_POST['pass']))
{
	$nombre = espulga_cadena($_POST['nombre']);
	$nick = espulga_cadena($_POST['nick']);
	$pass = espulga_cadena($_POST['pass']);

	$cadena_resultado = "<p style='color: red;'>El nick del cliente ya está registrado</p>";

	if(count(buscar($nick, 'nick', $_SESSION['datos']['cliente'])) == 0)
	{
		$cod_cliente = random(array_keys($_SESSION['datos']['cliente']));

		$cliente = [
			'cod_cliente' => $cod_cliente,
			'cif_dni' => '',
			'razon_social' => '',
			'domicilio_social' => '',
			'ciudad' => '',
			'email' => '',
			'telefono' => '',
			'nombre' => $nombre,
			'nick' => $nick,
			'pass' => $pass
		];

		$_SESSION['datos']['cliente'][$cod_cliente] = $cliente;

		$cadena_resultado = "<p style='color: green;'>Cliente registrado correctamente</p>";
	}
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Alta Cliente</title>
</head>
<body>
	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<div>
		<label for="nick">Nombre del usuario: </label>
		<input type="text" name="nombre" id="nombre" required>
		</div>

		<div>
		<label for="nick">Nick del cliente: </label>
		<input type="text" name="nick" id="nick" required>
		</div>

		<div>
		<label for="pass">Contraseña: </label>
		<input type="password" name="pass" id="pass" required>
		</div>

		<input type="submit" name="confirmar" value="Dar de alta">
	</form>

	<?=muestra_volver('menu.php')?>

	<?=$cadena_resultado?>
</body>
</html>
