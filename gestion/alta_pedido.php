<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

if(isset($_POST['fin']))
{
	$cod_pedido = espulga_numero($_POST['cod_pedido'], FILTER_VALIDATE_INT);
	$_SESSION['datos']['pedido'][$cod_pedido]['fecha'] = date('Y-m-d H:i:s');

	header('location:menu.php');
	exit();
}

if(!isset($_POST['cod_cliente']))
{
	header('location:realiza_pedido.php');
	exit();
}
else
{
	$cod_cliente = espulga_numero($_POST['cod_cliente'], FILTER_VALIDATE_INT);
}

if(!isset($_POST['cod_pedido']))
{
	$cod_pedido = random(array_keys($_SESSION['datos']['pedido']));
}

if(isset($_POST['articulo'], $_POST['cantidad'], $_POST['cod_pedido']))
{
	$articulo = $_SESSION['datos']['articulo'][espulga_numero($_POST['articulo'], FILTER_VALIDATE_INT)];
	$cantidad = espulga_numero($_POST['cantidad'], FILTER_VALIDATE_FLOAT);
	$cod_pedido = espulga_numero($_POST['cod_pedido'], FILTER_VALIDATE_INT);

	$precio = $articulo['precio'];
//	$precio_total = $precio * $cantidad;

	if(!isset($_SESSION['datos']['pedido'][$cod_pedido]))
	{
		$pedido = [
			'cod_pedido' => $cod_pedido,
			'fecha' => '',
			'cod_cliente' => $cod_cliente,
			'linea_pedido' => []
		];

		$_SESSION['datos']['pedido'][$cod_pedido] = $pedido;
	}

	$cod_articulo = $articulo['cod_articulo'];
	$lineas_pedido = $_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'];
	$linea_repetida = buscar($cod_articulo, 'cod_articulo', $lineas_pedido);

	if(count($linea_repetida) != 0)
	{
		$posicion = $linea_repetida[0]['num_linea_pedido'];
//		$_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'][$posicion]['precio'] += $precio_total;
		$_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'][$posicion]['cantidad'] += $cantidad;
	}
	else
	{
		$posicion = count($lineas_pedido) + 1;

		$linea_pedido = [
			'num_linea_pedido' => $posicion,
			'cod_pedido' => $cod_pedido,
//			'precio' => $precio_total,
			'precio' => $precio,
			'cantidad' => $cantidad,
			'cantidad_albaran' => 0,
			'cod_articulo' => $cod_articulo,
			'cod_usuario_gestion' => $_SESSION['usuario_gestion']['cod_usuario_gestion']
		];

		$_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'][$posicion] = $linea_pedido;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Realizar pedido</title>
</head>
<body>
	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<label for="articulo">Articulo: </label>
		<select id="articulo" name="articulo" required>
<?php
foreach($_SESSION['datos']['articulo'] as $articulo)
{
	echo "<option value='{$articulo['cod_articulo']}'>'{$articulo['nombre']}' - {$articulo['precio']}</option>";
}
?>
		</select>

		<label for="cantidad">Cantidad: </label>
		<input type="number" id="cantidad" name="cantidad" min="0">

		<input type="hidden" id="cod_cliente" name="cod_cliente" value='<?=$cod_cliente?>'>
		<input type="hidden" id="cod_pedido" name="cod_pedido" value='<?=$cod_pedido?>'>

		<input type="submit" value="Anyadir al pedido">
	</form>
<?php
if(isset($_SESSION['datos']['pedido'][$cod_pedido]))
{
?>
	<table border="1">
		<caption>Pedido #<?=$cod_pedido?></caption>
		<thead>
			<th>Linea</th>
			<th>Articulo</th>
			<th>Cantidad</th>
			<th>Precio</th>
		</thead>
		<tbody>
<?php
	foreach($_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'] as $linea_pedido)
	{
		echo '<tr>';
		echo "<td>{$linea_pedido['num_linea_pedido']}</td>";
		echo "<td>{$_SESSION['datos']['articulo'][$linea_pedido['cod_articulo']]['nombre']}</td>";
		echo "<td>{$linea_pedido['cantidad']}</td>";
		echo "<td>{$linea_pedido['precio']}</td>";
		echo '</tr>';
	}
?>
		</tbody>
	</table>

	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<input type="hidden" id="cod_pedido" name="cod_pedido" value='<?=$cod_pedido?>'>
		<input type="submit" name="fin" value="Terminar">
	</form>
<?php
}
?>
	<?=muestra_volver('menu.php')?>
</body>
</html>
