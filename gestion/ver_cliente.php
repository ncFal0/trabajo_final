<?php
require_once '../funciones.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver Cliente</title>
</head>
<body>
	<table id="tabla" border="1">
		<thead id="cabecera">
			<th>Código</th>
			<th>CIF</th>
			<th>Razon Social</th>
			<th>Domicilio Social</th>
			<th>Ciudad</th>
			<th>Email</th>
			<th>Telefono</th>
			<th>Nombre</th>
			<th>Nick</th>
			<th>Pass</th>
		</thead>

		<tbody id="cuerpo">
		</tbody>
	</table>

	<?=muestra_volver('menu.php')?>

<script type="module">
import {listar} from '../ajax.js';
listar('../ajax.php?datos=1', 'cliente');
</script>

</body>
</html>
