<?php
require_once '../funciones.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver Articulo</title>
</head>
<body>
	<table id="tabla" border="1">
		<thead id="cabecera">
			<th>Código</th>
			<th>Nombre</th>
			<th>Descripción</th>
			<th>Precio</th>
			<th>Descuento</th>
			<th>IVA</th>
		</thead>

		<tbody id="cuerpo">
		</tbody>
	</table>

	<?=muestra_volver('menu.php')?>

<script type="module">
import {listar} from '../ajax.js';
listar('../ajax.php?datos=1', 'articulo');
</script>

</body>
</html>
