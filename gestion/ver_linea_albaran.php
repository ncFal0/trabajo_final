<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

if(!isset($_REQUEST['codigo']))
{
	header('location:menu.php');
	exit();
}

$codigo = espulga_numero($_REQUEST['codigo'], FILTER_VALIDATE_INT);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver Albaranes</title>
</head>
<body>
	<table id="tabla" border="1">
	<caption id="titulo">Albarán #<?=$codigo?></caption>
		<thead id="cabecera">
			<th>Número</th>
			<th>Código Albarán</th>
			<th>Precio</th>
			<th>Cantidad</th>
			<th>Código Artículo</th>
			<th>Código Usuario Gestión</th>
			<th>Número Línea Pedido</th>
			<th>Código Pedido</th>
		</thead>

		<tbody id="cuerpo">
		</tbody>
	</table>

	<?=muestra_volver('menu.php')?>

<script type="module">
import {listar} from '../ajax.js';
listar('../ajax.php?codigo=<?=$codigo?>&entidad=albaran', 'linea_albaran');
</script>

</body>
</html>
