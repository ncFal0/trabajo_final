<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

if(!isset($_REQUEST['codigo']))
{
	header('location:menu.php');
	exit();
}

$codigo = espulga_numero($_REQUEST['codigo'], FILTER_VALIDATE_INT);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver Facturas</title>
</head>
<body>
	<table id="tabla" border="1">
	<caption id="titulo">Factura #<?=$codigo?></caption>
		<thead id="cabecera">
			<th>Número</th>
			<th>Código Factura</th>
			<th>Precio</th>
			<th>Cantidad</th>
			<th>Código Artículo</th>
			<th>Código Usuario Gestión</th>
			<th>Número Línea Albarán</th>
			<th>Código Albarán</th>
		</thead>

		<tbody id="cuerpo">
		</tbody>
	</table>

	<?=muestra_volver('menu.php')?>

<script type="module">
import {listar} from '../ajax.js';
listar('../ajax.php?codigo=<?=$codigo?>&entidad=factura', 'linea_factura');
</script>

</body>
</html>
