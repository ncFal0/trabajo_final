<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

$cadena_resultado = "";

if(isset($_POST['nombre'], $_POST['descripcion'], $_POST['precio']))
{
	$nombre = espulga_cadena($_POST['nombre']);
	$descripcion = espulga_cadena($_POST['descripcion']);
	$precio = espulga_numero($_POST['precio'], FILTER_VALIDATE_FLOAT);

	//$cadena_resultado = "<p style='color: red;'>El nick del usuario ya está registrado</p>";

	$cod_articulo = random(array_keys($_SESSION['datos']['articulo']));

	$articulo = [
		'cod_articulo' => $cod_articulo,
		'nombre' => $nombre,
		'descripcion' => $descripcion,
		'precio' => $precio,
		'descuento' => 0,
		'iva' => 21
	];

	$_SESSION['datos']['articulo'][$cod_articulo] = $articulo;

	$cadena_resultado = "<p style='color: green;'>Articulo registrado correctamente</p>";
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Alta Articulo</title>
</head>
<body>
	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<div>
		<label for="nick">Nombre del articulo: </label>
		<input type="text" name="nombre" id="nombre" required>
		</div>

		<div>
		<label for="descripcion">Descripcion del articulo: </label>
		</div>
		<textarea id="descripcion" name="descripcion" rows="6" cols="50"></textarea>

		<div>
		<label for="precio">Precio: </label>
		<input type="number" name="precio" id="precio" step="0.01" min="0" required>
		</div>

		<input type="submit" name="confirmar" value="Dar de alta">
	</form>

	<?=muestra_volver('menu.php')?>

	<?=$cadena_resultado?>
</body>
</html>
