<?php
require_once '../funciones.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver Usuario Gestion</title>
</head>
<body>
	<table id="tabla" border="1">
		<thead id="cabecera">
			<th>Código</th>
			<th>Nombre</th>
			<th>Nick</th>
			<th>Pass</th>
		</thead>

		<tbody id="cuerpo">
		</tbody>
	</table>

	<?=muestra_volver('menu.php')?>

<!--<script type="module" src="ver_usuario_gestion.js">-->
<script type="module">
import {listar} from '../ajax.js';
listar('../ajax.php?datos=1', 'usuario_gestion');
</script>

</body>
</html>
