<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

if(isset($_POST['fin']))
{
	$cod_albaran = espulga_numero($_POST['cod_albaran'], FILTER_VALIDATE_INT);
	$_SESSION['datos']['albaran'][$cod_albaran]['fecha'] = date('Y-m-d H:i:s');

	$albaran = $_SESSION['datos']['albaran'][$cod_albaran];
	$cod_pedido = $albaran['cod_pedido'];
	foreach($albaran['linea_albaran'] as $linea_albaran)
	{
		$num_linea = $linea_albaran['num_linea_pedido'];
		$cantidad = $linea_albaran['cantidad'];
		$_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'][$num_linea]['cantidad_albaran'] += $cantidad;
	}

	header('location:menu.php');
	exit();
}

if(!isset($_POST['cod_pedido']))
{
	header('location:realiza_albaran.php');
	exit();
}
else
{
	$cod_pedido = espulga_numero($_POST['cod_pedido'], FILTER_VALIDATE_INT);
}

if(!isset($_POST['cod_albaran']))
{
	$cod_albaran = random(array_keys($_SESSION['datos']['albaran']));
}

if(isset($_POST['num_linea_pedido'], $_POST['cantidad'], $_POST['cod_albaran']))
{
	$num_linea_pedido = espulga_numero($_POST['num_linea_pedido'], FILTER_VALIDATE_INT);
	$cantidad = espulga_numero($_POST['cantidad'], FILTER_VALIDATE_FLOAT);
	$cod_albaran = espulga_numero($_POST['cod_albaran'], FILTER_VALIDATE_INT);
	
	$linea_pedido = $_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'][$num_linea_pedido];

	$articulo = $_SESSION['datos']['articulo'][$linea_pedido['cod_articulo']];

	$precio = $articulo['precio'];
//	$precio_total = $precio * $cantidad;

	if(!isset($_SESSION['datos']['albaran'][$cod_albaran]))
	{
		$albaran = [
			'cod_albaran' => $cod_albaran,
			'fecha' => '',
			'facturado' => 0,
			'cod_cliente' => $_SESSION['datos']['pedido'][$cod_pedido]['cod_cliente'],
			'cod_pedido' => $cod_pedido,
			'linea_albaran' => []
		];

		$_SESSION['datos']['albaran'][$cod_albaran] = $albaran;
	}

	$cod_articulo = $articulo['cod_articulo'];
	$lineas_albaran = $_SESSION['datos']['albaran'][$cod_albaran]['linea_albaran'];
	$linea_repetida = buscar($cod_articulo, 'cod_articulo', $lineas_albaran);

	if(count($linea_repetida) != 0)
	{
		if($cantidad + $linea_repetida[0]['cantidad'] > $linea_pedido['cantidad'])
		{
			$cantidad = $linea_repetida[0]['cantidad'];
//			$precio_total = $precio * $cantidad;
		}

		$posicion = $linea_repetida[0]['num_linea_pedido'];
//		$_SESSION['datos']['albaran'][$cod_albaran]['linea_albaran'][$posicion]['precio'] += $precio_total;
		$_SESSION['datos']['albaran'][$cod_albaran]['linea_albaran'][$posicion]['cantidad'] += $cantidad;
	}
	else
	{
		$posicion = count($lineas_albaran) + 1;

		$linea_albaran = [
			'num_linea_albaran' => $posicion,
			'cod_albaran' => $cod_albaran,
//			'precio' => $precio_total,
			'precio' => $precio,
			'cantidad' => $cantidad,
			'cod_articulo' => $cod_articulo,
			'cod_usuario_gestion' => $_SESSION['usuario_gestion']['cod_usuario_gestion'],
			'num_linea_pedido' => $num_linea_pedido,
			'cod_pedido' => $cod_pedido
		];

		$_SESSION['datos']['albaran'][$cod_albaran]['linea_albaran'][$posicion] = $linea_albaran;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Realizar albaran</title>
</head>
<body>
	<table border="1">
		<caption>Pedido #<?=$cod_pedido?></caption>
		<thead>
			<th>Linea</th>
			<th>Articulo</th>
			<th>Cantidad</th>
			<th>Precio</th>
			<th>Cantidad Albaran</th>
		</thead>
		<tbody>
<?php
$lineas_pedido = $_SESSION['datos']['pedido'][$cod_pedido]['linea_pedido'];
foreach($lineas_pedido as $linea_pedido)
{
	$cantidad = $linea_pedido['cantidad'];
	$cantidad_albaran = $linea_pedido['cantidad_albaran'];

	if(isset($_SESSION['datos']['albaran'][$cod_albaran]) && $cantidad_albaran < $cantidad)
	{

		$cod_articulo = $linea_pedido['cod_articulo'];
		$lineas_albaran = $_SESSION['datos']['albaran'][$cod_albaran]['linea_albaran'];
		$linea_albaran = buscar($cod_articulo, 'cod_articulo', $lineas_albaran);
		$cantidad_albaran += (count($linea_albaran) > 0) ? $linea_albaran[0]['cantidad'] : 0;
		$cantidad_albaran = ($cantidad_albaran > $cantidad) ? $cantidad : $cantidad_albaran;
	}

	$cantidad_restante = $cantidad - $cantidad_albaran;

	echo '<tr>';
	echo "<td>{$linea_pedido['num_linea_pedido']}</td>";
	echo "<td>{$_SESSION['datos']['articulo'][$linea_pedido['cod_articulo']]['nombre']}</td>";
	echo "<td>{$cantidad}</td>";
	echo "<td>{$linea_pedido['precio']}</td>";
	echo "<td>{$cantidad_albaran}</td>";

	if($cantidad_restante > 0)
	{
?>
	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<td>
		<input type="number" id="cantidad" name="cantidad" min="0" max='<?=$cantidad_restante?>' value="0">
		</td>

		<input type="hidden" name="cod_pedido" value='<?=$cod_pedido?>'>
		<input type="hidden" name="cod_albaran" value='<?=$cod_albaran?>'>
		<input type="hidden" name="num_linea_pedido" value='<?=$linea_pedido['num_linea_pedido']?>'>

		<td>
		<input type="submit" value="Anyadir al albaran">
		</td>
	</form>
<?php
	}
	echo '</tr>';
}
?>
		</tbody>
	</table>

<?php
if(isset($_SESSION['datos']['albaran'][$cod_albaran]))
{
?>
	<table border="1">
		<caption>Albaran #<?=$cod_albaran?></caption>
		<thead>
			<th>Linea</th>
			<th>Articulo</th>
			<th>Cantidad</th>
			<th>Precio</th>
		</thead>
		<tbody>
<?php
	foreach($_SESSION['datos']['albaran'][$cod_albaran]['linea_albaran'] as $linea_albaran)
	{
	
		echo '<tr>';
		echo "<td>{$linea_albaran['num_linea_albaran']}</td>";
		echo "<td>{$_SESSION['datos']['articulo'][$linea_albaran['cod_articulo']]['nombre']}</td>";
		echo "<td>{$linea_albaran['cantidad']}</td>";
		echo "<td>{$linea_albaran['precio']}</td>";
		echo '</tr>';
	}
?>
		</tbody>
	</table>

	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<input type="hidden" name="cod_albaran" value='<?=$cod_albaran?>'>
		<input type="submit" name="fin" value="Terminar">
	</form>
<?php
}
?>
	<?=muestra_volver('menu.php')?>
</body>
</html>
