<?php
require_once '../funciones.php';

function codigos_albaran(array $factura): array
{
	$codigos_albaran = [];

	foreach($factura['linea_factura'] as $linea_factura)
	{
		if(!in_array($linea_factura['cod_albaran'], $codigos_albaran))
		{
			$codigos_albaran[] = $linea_factura['cod_albaran'];
		}
	}

	return $codigos_albaran;
}

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

// realiza la factura dandole una fecha de creacion
// las facturas a medio hacer (que no tengan fecha) se borraran automaticamente en '../guardar.php'
if(isset($_POST['fin']))
{
	$cod_factura = espulga_numero($_POST['cod_factura'], FILTER_VALIDATE_INT);
	$_SESSION['datos']['factura'][$cod_factura]['fecha'] = date('Y-m-d H:i:s');

	$codigos_albaran = codigos_albaran($_SESSION['datos']['factura'][$cod_factura]);
	foreach($codigos_albaran as $cod_albaran)
	{
		$_SESSION['datos']['albaran'][$cod_albaran]['facturado'] = 1;
	}

	header('location:menu.php');
	exit();
}

// comprobar si recibe el código del cliente que ha sido escogido en 'realiza_factura.php'
if(!isset($_POST['cod_cliente']))
{
	header('location:realiza_factura.php');
	exit();
}
// si lo recibe lo vuelca en una variable
else
{
	$cod_cliente = espulga_numero($_POST['cod_cliente'], FILTER_VALIDATE_INT);
}

// crear un nuevo código para identificar la factura en caso de ser nueva
if(!isset($_POST['cod_factura']))
{
	$cod_factura = random(array_keys($_SESSION['datos']['factura']));
}

if(isset($_POST['cod_albaran'], $_POST['cod_factura']))
{
	$cod_albaran = espulga_numero($_POST['cod_albaran'], FILTER_VALIDATE_INT);
	$cod_factura = espulga_numero($_POST['cod_factura'], FILTER_VALIDATE_INT);

	$albaran = $_SESSION['datos']['albaran'][$cod_albaran];

	$codigos_albaran = [];
	if(isset($_SESSION['datos']['factura'][$cod_factura])) {
		$codigos_albaran = codigos_albaran($_SESSION['datos']['factura'][$cod_factura]);
	}
	$previo_factura = in_array($cod_albaran, $codigos_albaran);


	if(!$albaran['facturado'] && !$previo_factura)
	{
		if(!isset($_SESSION['datos']['factura'][$cod_factura]))
		{
			$factura = [
				'cod_factura' => $cod_factura,
				'fecha' => '',
				'descuento_factura' => 0,
				'cod_cliente' => $cod_cliente,
				'linea_factura' => []
			];

			$_SESSION['datos']['factura'][$cod_factura] = $factura;
		}

		foreach($albaran['linea_albaran'] as $linea_albaran)
		{
			$posicion = count($_SESSION['datos']['factura'][$cod_factura]['linea_factura']) + 1;

			$linea_factura = [
				'num_linea_factura' => $posicion,
				'cod_factura' => $cod_factura,
				'precio' => $linea_albaran['precio'],
				'cantidad' => $linea_albaran['cantidad'],
				'cod_articulo' => $linea_albaran['cod_articulo'],
				'cod_usuario_gestion' => $_SESSION['usuario_gestion']['cod_usuario_gestion'],
				'num_linea_albaran' => $linea_albaran['num_linea_albaran'],
				'cod_albaran' => $cod_albaran
			];

			$_SESSION['datos']['factura'][$cod_factura]['linea_factura'][$posicion] = $linea_factura;
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Realizar albaran</title>
</head>
<body>
	<table border="1">
		<caption>Cliente <?=$_SESSION['datos']['cliente'][$cod_cliente]['nombre']?></caption>
		<thead>
			<th>Albaran</th>
			<th>De Pedido</th>
			<th>Fecha</th>
			<th>Facturado</th>
		</thead>
		<tbody>
<?php
$albaranes = buscar($cod_cliente, 'cod_cliente', $_SESSION['datos']['albaran']);
foreach($albaranes as $albaran)
{
	$facturado = ($albaran['facturado'] == 0) ? false : true;

	$codigos_albaran = [];
	if(isset($_SESSION['datos']['factura'][$cod_factura])) {
		$codigos_albaran = codigos_albaran($_SESSION['datos']['factura'][$cod_factura]);
	}
	$previo_factura = in_array($albaran['cod_albaran'], $codigos_albaran);

	echo '<tr>';
	echo "<td>{$albaran['cod_albaran']}</td>";
	echo "<td>{$albaran['cod_pedido']}</td>";
	echo "<td>{$albaran['fecha']}</td>";
	echo '<td>' . (($facturado | $previo_factura) ? 'si' : 'no') . '</td>';

	if(!$facturado && !$previo_factura)
	{
?>
	<td>
		<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
			<input type="hidden" name="cod_cliente" value='<?=$cod_cliente?>'>
			<input type="hidden" name="cod_factura" value='<?=$cod_factura?>'>
			<input type="hidden" name="cod_albaran" value='<?=$albaran['cod_albaran']?>'>

			<input type="submit" value="Anyadir a la factura">
		</form>
	</td>
<?php
	}
	echo '</tr>';
}
?>
		</tbody>
	</table>

<?php
// muestra los albaranes facturados por el momento una vez se anyade uno a la factura
if(isset($_SESSION['datos']['factura'][$cod_factura]))
{
?>
	<table border="1">
		<caption>Factura #<?=$cod_factura?></caption>
		<thead>
			<th>Albaran</th>
			<th>De Pedido</th>
			<th>Fecha</th>
		</thead>
		<tbody>
<?php
	$codigos_albaran = codigos_albaran($_SESSION['datos']['factura'][$cod_factura]);

	foreach($codigos_albaran as $codigo)
	{
		echo '<tr>';
		echo "<td>{$codigo}</td>";
		echo "<td>{$_SESSION['datos']['albaran'][$codigo]['cod_pedido']}</td>";
		echo "<td>{$_SESSION['datos']['albaran'][$codigo]['fecha']}</td>";
		echo '</tr>';
	}
?>
		</tbody>
	</table>

	<form action='<?=$_SERVER['PHP_SELF']?>' method="POST">
		<input type="hidden" name="cod_factura" value='<?=$cod_factura?>'>
		<input type="submit" name="fin" value="Terminar">
	</form>
<?php
}
?>
	<?=muestra_volver('menu.php')?>
</body>
</html>
