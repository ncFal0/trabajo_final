<?php
require_once '../funciones.php';

session_start();

if(!isset($_SESSION['usuario_gestion']))
{
	header('location:../login.php');
	exit();
}

if(!isset($_POST['cod_pedido']))
{
	header('location:menu.php');
	exit();
}

$codigo = espulga_numero($_POST['cod_pedido'], FILTER_VALIDATE_INT);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Ver Albaranes</title>
</head>
<body>
	<table id="tabla" border="1">
<!--		<caption id="titulo"></caption>-->
		<thead id="cabecera">
			<th>Código</th>
			<th>Fecha</th>
			<th>Facturado</th>
			<th>Código Cliente</th>
			<th>Código Pedido</th>
			<th>Lineas Albarán</th>
		</thead>

		<tbody id="cuerpo">
		</tbody>
	</table>

	<?=muestra_volver('menu.php')?>

<script type="module">
import {listar} from '../ajax.js';
listar('../ajax.php?codigo=<?=$codigo?>&campo=cod_pedido&entidad=albaran', 'albaran');
</script>

</body>
</html>
