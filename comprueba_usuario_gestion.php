<?php
require_once 'funciones.php';

session_start();

if(isset($_POST['cerrar_sesion']))
{
	$_SESSION = [];
	session_destroy();
}

if(!isset($_POST['nick'], $_POST['pass']))
{
	header('location:login.php');
	exit();
}
else
{
	$datos = cargar_datos();
	
	$nick = espulga_cadena($_POST['nick']);
	$pass = espulga_cadena($_POST['pass']);

	$usuario_gestion = buscar($nick, 'nick', $datos['usuario_gestion']);
	
	if(count($usuario_gestion) == 0)
	{
		volver_intentar(0);
	}
	else
	{
		if($usuario_gestion[0]['pass'] != $pass)
		{
			volver_intentar(1);
		}
		else
		{
			$_SESSION['datos'] = $datos;
			$_SESSION['usuario_gestion'] = $usuario_gestion[0];

			header('location:gestion/menu.php');
			exit();	
		}
	}
}
?>
