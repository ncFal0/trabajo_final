DROP DATABASE IF EXISTS trabajo_daw;

CREATE DATABASE trabajo_daw
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

USE trabajo_daw;

CREATE TABLE usuario_gestion(
	cod_usuario_gestion BIGINT UNSIGNED NOT NULL PRIMARY KEY,
	nombre VARCHAR(128),
	nick VARCHAR(128) UNIQUE NOT NULL,
	pass VARCHAR(128) NOT NULL
);

INSERT INTO usuario_gestion VALUES('0','gestor','gestor','1234');

CREATE TABLE cliente(
	cod_cliente BIGINT UNSIGNED NOT NULL PRIMARY KEY,
	cif_dni VARCHAR(9),
	razon_social VARCHAR(150),
	domicilio_social VARCHAR(150),
	ciudad VARCHAR(150),
	email VARCHAR(150),
	telefono VARCHAR(15),
	nombre VARCHAR(150),
	nick VARCHAR(128) UNIQUE NOT NULL,
	pass VARCHAR(128) NOT NULL
);

CREATE TABLE articulo(
	cod_articulo BIGINT UNSIGNED NOT NULL PRIMARY KEY,
	nombre VARCHAR(150),
	descripcion VARCHAR(300),
	precio DECIMAL(10,2),
	descuento TINYINT UNSIGNED,
	iva TINYINT UNSIGNED
);

CREATE TABLE pedido(
	cod_pedido BIGINT UNSIGNED NOT NULL PRIMARY KEY,
	fecha DATETIME,
	cod_cliente BIGINT UNSIGNED NOT NULL,
	CONSTRAINT fk_ped_cli FOREIGN KEY(cod_cliente)
	REFERENCES cliente(cod_cliente) ON DELETE NO ACTION ON UPDATE CASCADE
);

CREATE TABLE albaran(
	cod_albaran BIGINT UNSIGNED NOT NULL PRIMARY KEY,
	fecha DATETIME,
--	concepto VARCHAR(300),
	facturado BOOLEAN,
	cod_cliente BIGINT UNSIGNED NOT NULL,
	cod_pedido BIGINT UNSIGNED NOT NULL,
	CONSTRAINT fk_alb_cli FOREIGN KEY(cod_cliente)
	REFERENCES cliente(cod_cliente) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_alb_ped FOREIGN KEY(cod_pedido)
	REFERENCES pedido(cod_pedido) ON DELETE NO ACTION ON UPDATE CASCADE
);

CREATE TABLE factura(
	cod_factura BIGINT UNSIGNED NOT NULL PRIMARY KEY,
	fecha DATETIME,
	descuento_factura TINYINT UNSIGNED,
--	concepto VARCHAR(300),
	cod_cliente BIGINT UNSIGNED NOT NULL,
	CONSTRAINT fk_fac_cli FOREIGN KEY(cod_cliente)
	REFERENCES cliente(cod_cliente) ON DELETE NO ACTION ON UPDATE CASCADE
);

CREATE TABLE linea_pedido(
	num_linea_pedido INT NOT NULL,
	cod_pedido BIGINT UNSIGNED NOT NULL,
	precio DECIMAL(10,2),
	cantidad DECIMAL(10,2),
	cantidad_albaran DECIMAL(10,2),
	cod_articulo BIGINT UNSIGNED NOT NULL,
	cod_usuario_gestion BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY(num_linea_pedido, cod_pedido),
	CONSTRAINT fk_lin_ped_ped FOREIGN KEY(cod_pedido)
	REFERENCES pedido(cod_pedido) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_ped_art FOREIGN KEY(cod_articulo)
	REFERENCES articulo(cod_articulo) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_ped_usu_ges FOREIGN KEY(cod_usuario_gestion)
	REFERENCES usuario_gestion(cod_usuario_gestion) ON DELETE NO ACTION ON UPDATE CASCADE
);

CREATE TABLE linea_albaran(
	num_linea_albaran INT NOT NULL,
	cod_albaran BIGINT UNSIGNED NOT NULL,
	precio DECIMAL(10,2),
	cantidad DECIMAL(10,2),
	cod_articulo BIGINT UNSIGNED NOT NULL,
	cod_usuario_gestion BIGINT UNSIGNED NOT NULL,
	num_linea_pedido INT NOT NULL,
	cod_pedido BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY(num_linea_albaran, cod_albaran),
	CONSTRAINT fk_lin_alb_alb FOREIGN KEY(cod_albaran)
	REFERENCES albaran(cod_albaran) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_alb_art FOREIGN KEY(cod_articulo)
	REFERENCES articulo(cod_articulo) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_alb_usu_ges FOREIGN KEY(cod_usuario_gestion)
	REFERENCES usuario_gestion(cod_usuario_gestion) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_alb_lin_ped FOREIGN KEY(num_linea_pedido, cod_pedido)
	REFERENCES linea_pedido(num_linea_pedido, cod_pedido) ON DELETE NO ACTION ON UPDATE CASCADE
);

CREATE TABLE linea_factura(
	num_linea_factura INT NOT NULL,
	cod_factura BIGINT UNSIGNED NOT NULL,
	precio DECIMAL(10,2),
	cantidad DECIMAL(10,2),
	cod_articulo BIGINT UNSIGNED NOT NULL,
	cod_usuario_gestion BIGINT UNSIGNED NOT NULL,
	num_linea_albaran INT NOT NULL,
	cod_albaran BIGINT UNSIGNED NOT NULL,
	PRIMARY KEY(num_linea_factura, cod_factura),
	CONSTRAINT fk_lin_fac_fac FOREIGN KEY(cod_factura)
	REFERENCES factura(cod_factura) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_fac_art FOREIGN KEY(cod_articulo)
	REFERENCES articulo(cod_articulo) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_fac_usu_ges FOREIGN KEY(cod_usuario_gestion)
	REFERENCES usuario_gestion(cod_usuario_gestion) ON DELETE NO ACTION ON UPDATE CASCADE,
	CONSTRAINT fk_lin_fac_lin_alb FOREIGN KEY(num_linea_albaran, cod_albaran)
	REFERENCES linea_albaran(num_linea_albaran, cod_albaran) ON DELETE NO ACTION ON UPDATE CASCADE
);
