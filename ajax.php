<?php
require_once 'funciones.php';

session_start();

if(!isset($_SESSION['datos']))
{
	header('location:login.php');
	exit();
}


if(isset($_REQUEST['codigo'], $_REQUEST['campo'], $_REQUEST['entidad']))
{
	$codigo = espulga_numero($_REQUEST['codigo'], FILTER_VALIDATE_INT);
	$campo = espulga_cadena($_REQUEST['campo']);
	$entidad = espulga_cadena($_REQUEST['entidad']);

	$resultado = buscar($codigo, $campo, $_SESSION['datos'][$entidad]);
	foreach($resultado as $indice => $dato)
	{
		$resultado[$dato["cod_{$entidad}"]] = $dato;
		unset($resultado[$indice]);
	}

	$respuesta = [
		$entidad => $resultado
	];

	echo json_encode($respuesta, JSON_OBJECT_AS_ARRAY);
}
else if(isset($_REQUEST['codigo'], $_REQUEST['entidad']))
{
	$codigo = espulga_numero($_REQUEST['codigo'], FILTER_VALIDATE_INT);
	$entidad = espulga_cadena($_REQUEST['entidad']);

	$lineas = $_SESSION['datos'][$entidad][$codigo];
	echo json_encode($lineas, JSON_OBJECT_AS_ARRAY);
}
else if(isset($_REQUEST['datos']))
{
	echo json_encode($_SESSION['datos'], JSON_OBJECT_AS_ARRAY);
}
