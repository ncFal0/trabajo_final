export async function recoger(url)
{
	let datos = null;

	let respuesta = await fetch(url);
	if(!respuesta.ok)
	{
		throw new Error('Error!' + respuesta.status);
	}
	else
	{
		datos = await respuesta.json();
	}

	return datos;
}

export async function listar(url, nombre_entidad)
{
	recoger(url)
	.then(datos => {
		let tabla = document.getElementById('tabla');
		let cuerpo = document.getElementById('cuerpo');

		const entidad = datos[nombre_entidad];

		let objeto;
		let campo;

		let fila;
		let celda;
		let contenido;

		let cod_objeto;
		let nombre_campo;

		for(cod_objeto in entidad)
		{
			objeto = entidad[cod_objeto];

			fila = document.createElement('tr');

			for(nombre_campo in objeto)
			{
				campo = objeto[nombre_campo];

				celda = document.createElement('td');

				if(typeof campo === 'object' && campo !== null)
				{
					contenido = document.createElement('a');
					contenido.href = 'ver_linea_' + nombre_entidad + '.php?codigo=' + cod_objeto;
					contenido.appendChild(document.createTextNode('Ver ' + nombre_entidad));
				}
				else
				{
					contenido = document.createTextNode(campo);
				}

				celda.id = cod_objeto + ':' + nombre_campo;
				celda.appendChild(contenido);
				fila.appendChild(celda);
			}

			cuerpo.appendChild(fila);
		}
	})
	.catch(error => error.message);
}
