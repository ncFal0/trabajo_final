<?php

const HOST = "localhost";
const DBNAME = "trabajo_daw";
const USER = "jmanuel";
const PASSWD = "";

function conexion(): ?PDO
{
	$conexion = null;
	try
	{
		$opciones = [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_CASE => PDO::CASE_LOWER
		];

		$dsn = 'mysql:host=' . HOST . '; dbname=' . DBNAME;
		$conexion = new PDO($dsn, USER, PASSWD, $opciones);
	}
	catch (Exception $excepcion)
	{
		echo $excepcion->getMessage();
	}

	return $conexion;
}


function format_string(array $datos, bool $valor = false): string
{
	$cadena = '';
	$numero = count($datos);

	for($i = 0; $i < $numero; $i++)
	{
		if($valor)
			$cadena .= ':' . $datos[$i];
		else
			$cadena .= $datos[$i];

		if($i != $numero - 1)
		{
			$cadena .= ',';
		}
	}

	return $cadena;
}

function format_array(array $datos): array
{
	$array = [];

	foreach($datos as $indice => $valor)
	{
		$array[":{$indice}"] = $valor;
	}

	return $array;
}

function insertar(PDO $conexion, string $tabla, array $datos)
{
	$indices = format_string(array_keys($datos));
	$indices_prepared = format_string(array_keys($datos), true);

	$sql = "INSERT INTO {$tabla}({$indices}) VALUES ({$indices_prepared})";

	$consulta = $conexion->prepare($sql);
	$consulta->execute(format_array($datos));
}

function borrar(PDO $conexion, string $tabla)
{
	$sql = "DELETE FROM {$tabla}";
	$consulta = $conexion->prepare($sql);
	$consulta->execute();
}

function recoger(PDO $conexion, string $tabla): array
{
	$sql = "SELECT * FROM {$tabla}";
	$consulta = $conexion->prepare($sql);
	$consulta->execute();
	return $consulta->fetchAll(PDO::FETCH_ASSOC);
}

function random(array $lista_cod): int
{
	do
	{
		$cod = rand(0, PHP_INT_MAX);
	}
	while(in_array($cod, $lista_cod));

	return $cod;
}

//FUNCIONES PARA EL LOGIN

function volver_intentar(string $codigo) {
	$mensajes = [
		"El nombre de usuario no está registrado",
		"La contraseña es incorrecta"
	];

	echo "<form action=\"login.php\" method=\"POST\">";
	echo "<p>{$mensajes[$codigo]}</p>";
	echo "<input type=\"submit\" value=\"Volver a intentar\">";
	echo "</form>";
}

function muestra_volver(string $pagina) {
	?>
	<form action='<?=$pagina?>' method="POST">
		<input type="submit" name="volver" value="Volver">
	</form>
	<?php
}

//FUNCIONES NUEVAS

function buscar(string|int $texto, string $campo, array $entidades): array
{
	$resultado = [];

	foreach($entidades as $entidad)
	{
		if($entidad[$campo] == $texto)
		{
			$resultado[] = $entidad;
		}
	}

	return $resultado;
}

function buscar_indice(string $texto, string $campo, array $entidades): array
{
	$resultado = [];

	foreach($entidades as $indice => $entidad)
	{
		if($entidad[$campo] == $texto)
		{
			$resultado[] = $indice;
		}
	}

	return $resultado;
}

function vectoriza_entidades(string $campo_clave, array $entidades): array
{
	$vector_entidades = [];

	foreach($entidades as $entidad)
	{
		$vector_entidades[$entidad[$campo_clave]] = $entidad;
	}

	return $vector_entidades;
}

function meter_lineas(string $nombre, array $vector_entidades, array $lineas): array
{
	$codigos = array_keys($vector_entidades);

	foreach($lineas as $linea)
	{
		$codigo_entidad = $linea["cod_{$nombre}"];
		$numero_linea = $linea["num_linea_{$nombre}"];

		if(in_array($codigo_entidad, $codigos))
		{
			$vector_entidades[$codigo_entidad]["linea_{$nombre}"][$numero_linea] = $linea;
		}
	}

	return $vector_entidades;
}

function sacar_lineas(string $nombre, array $vector_entidades): array
{
	$lineas = [];

	foreach($vector_entidades as $entidad)
	{
		foreach($entidad["linea_{$nombre}"] as $linea)
		{
			$lineas[] = $linea;
		}
	}

	return $lineas;
}

function borrar_lineas(string $nombre, array $vector_entidades): array
{
	$vector_resultado = [];

	foreach($vector_entidades as $entidad)
	{
		unset($entidad["linea_{$nombre}"]);
		$vector_resultado[] = $entidad;
	}

	return $vector_resultado;
}

function cargar_datos(): ?array
{
	$datos = [];

	try
	{
		$conexion = conexion();

		$usuario_gestion = recoger($conexion, 'usuario_gestion');
		$cliente = recoger($conexion, 'cliente');
		$articulo = recoger($conexion, 'articulo');
		$pedido = recoger($conexion, 'pedido');
		$albaran = recoger($conexion, 'albaran');
		$factura = recoger($conexion, 'factura');
		$linea_pedido = recoger($conexion, 'linea_pedido');
		$linea_albaran = recoger($conexion, 'linea_albaran');
		$linea_factura = recoger($conexion, 'linea_factura');
	}
	catch(Exception $excepcion)
	{
		echo $excepcion->getMessage();
		return null;
	}

	$conexion = null;


	$datos['usuario_gestion'] = vectoriza_entidades('cod_usuario_gestion', $usuario_gestion);
	$datos['cliente'] = vectoriza_entidades('cod_cliente', $cliente);
	$datos['articulo'] = vectoriza_entidades('cod_articulo', $articulo);
	$datos['pedido'] = meter_lineas('pedido', vectoriza_entidades('cod_pedido', $pedido), $linea_pedido);
	$datos['albaran'] = meter_lineas('albaran', vectoriza_entidades('cod_albaran', $albaran), $linea_albaran);
	$datos['factura'] = meter_lineas('factura', vectoriza_entidades('cod_factura', $factura), $linea_factura);

	return $datos;
}

function guardar_datos(array $datos)
{
	$datos['linea_pedido'] = sacar_lineas('pedido', $datos['pedido']);
	$datos['linea_albaran'] = sacar_lineas('albaran', $datos['albaran']);
	$datos['linea_factura'] = sacar_lineas('factura', $datos['factura']);
	$datos['pedido'] = borrar_lineas('pedido', $datos['pedido']);
	$datos['albaran'] = borrar_lineas('albaran', $datos['albaran']);
	$datos['factura'] = borrar_lineas('factura', $datos['factura']);

	try
	{
		$conexion = conexion();

		$conexion->beginTransaction();

		borrar($conexion, 'linea_factura');
		borrar($conexion, 'linea_albaran');
		borrar($conexion, 'linea_pedido');
		borrar($conexion, 'factura');
		borrar($conexion, 'albaran');
		borrar($conexion, 'pedido');
		borrar($conexion, 'articulo');
		borrar($conexion, 'cliente');
		borrar($conexion, 'usuario_gestion');

		foreach($datos as $nombre => $entidad)
		{
			foreach($entidad as $objeto)
			{
				insertar($conexion, $nombre, $objeto);
			}
		}

		$conexion->commit();
	}
	catch(Exception $excepcion)
	{
		$conexion->rollback();
		echo $excepcion->getMessage();
	}

	$conexion = null;
}

function espulga_cadena(string $cadena)
{
	$cadena = trim($cadena);
	$cadena = stripslashes($cadena);
	$cadena = htmlspecialchars($cadena);

	return $cadena;
}

function espulga_numero(int|float $numero, int $opcion)
{
	return (!filter_var($numero, $opcion)) ? 0 : $numero;
}
