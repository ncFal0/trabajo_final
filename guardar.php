<?php
require_once 'funciones.php';

function borrar_sin_fecha(array $datos)
{
	foreach($datos as $indice => $entidad)
	{
		if($entidad['fecha'] == '' || $entidad['fecha'] == null)
		{
			unset($datos[$indice]);
		}
	}

	return $datos;
}

session_start();

if(!isset($_SESSION['datos']))
{
	header('location:login.php');
	exit();
}

$datos = $_SESSION['datos'];
$datos['pedido'] = borrar_sin_fecha($datos['pedido']);
$datos['albaran'] = borrar_sin_fecha($datos['albaran']);
$datos['factura'] = borrar_sin_fecha($datos['factura']);

guardar_datos($datos);

muestra_volver('gestion/menu.php');
?>
